{
    "id": "732a8dfc-e161-4f3a-af34-3087f19afc40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_asteroid_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11cb1a35-9010-4537-a269-26839da67c04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "732a8dfc-e161-4f3a-af34-3087f19afc40",
            "compositeImage": {
                "id": "0abd4dc9-7829-4725-ad5f-9a30fdf17636",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11cb1a35-9010-4537-a269-26839da67c04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8340b25b-1fb9-46c7-ab1a-d7f5e71f497a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11cb1a35-9010-4537-a269-26839da67c04",
                    "LayerId": "a339dae2-4de4-4389-8b4f-9849aff4c4e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a339dae2-4de4-4389-8b4f-9849aff4c4e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "732a8dfc-e161-4f3a-af34-3087f19afc40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}