{
    "id": "e9f90d68-9f24-4b66-abaa-467c227c9c87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_ship_3_lives",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "42114f53-9f55-4ba8-a16f-9bf41c70a049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9f90d68-9f24-4b66-abaa-467c227c9c87",
            "compositeImage": {
                "id": "a0f86c87-7f58-462a-b3dd-b10e68619347",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42114f53-9f55-4ba8-a16f-9bf41c70a049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "591a3822-dabf-4b1d-82e2-21fc0276aa8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42114f53-9f55-4ba8-a16f-9bf41c70a049",
                    "LayerId": "46ef43cb-cda5-4f07-bf70-593e60611a4f"
                }
            ]
        },
        {
            "id": "5e1ab01f-c200-4937-89df-8746568cff24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9f90d68-9f24-4b66-abaa-467c227c9c87",
            "compositeImage": {
                "id": "df724198-b484-47b2-8bf6-13389d0cb8a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e1ab01f-c200-4937-89df-8746568cff24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1892fd5-77e7-4d7f-a8bb-bc4eafe133cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e1ab01f-c200-4937-89df-8746568cff24",
                    "LayerId": "46ef43cb-cda5-4f07-bf70-593e60611a4f"
                }
            ]
        },
        {
            "id": "e7f7d168-e166-418d-8fac-e5a3a2811950",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9f90d68-9f24-4b66-abaa-467c227c9c87",
            "compositeImage": {
                "id": "fd22394e-f1c6-40ca-8a1d-b215b17c61af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7f7d168-e166-418d-8fac-e5a3a2811950",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4c5b6da-d5cd-48b3-b4b0-5d3bd78477d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7f7d168-e166-418d-8fac-e5a3a2811950",
                    "LayerId": "46ef43cb-cda5-4f07-bf70-593e60611a4f"
                }
            ]
        },
        {
            "id": "d5389031-e1a0-4420-bcea-079c455975de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9f90d68-9f24-4b66-abaa-467c227c9c87",
            "compositeImage": {
                "id": "b3c56782-e964-40d1-a9a6-4e4d58840d62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5389031-e1a0-4420-bcea-079c455975de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2c71de3-23bb-4bdc-98ab-6378e0cd0e1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5389031-e1a0-4420-bcea-079c455975de",
                    "LayerId": "46ef43cb-cda5-4f07-bf70-593e60611a4f"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "46ef43cb-cda5-4f07-bf70-593e60611a4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9f90d68-9f24-4b66-abaa-467c227c9c87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}