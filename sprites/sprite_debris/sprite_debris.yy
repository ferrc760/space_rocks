{
    "id": "9885bb5f-93ff-44f6-ab07-1fb101e9d1fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_debris",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0efbadfd-170f-49cb-b7ad-41359e900d72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9885bb5f-93ff-44f6-ab07-1fb101e9d1fe",
            "compositeImage": {
                "id": "1a9f84cd-909b-4a03-a337-dd9a7a6d5153",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0efbadfd-170f-49cb-b7ad-41359e900d72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bea4522-9ef9-42a4-86cd-0a22e21b1073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0efbadfd-170f-49cb-b7ad-41359e900d72",
                    "LayerId": "c9b0b9cf-b3c2-4a91-9844-50991a5bcd46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "c9b0b9cf-b3c2-4a91-9844-50991a5bcd46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9885bb5f-93ff-44f6-ab07-1fb101e9d1fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}