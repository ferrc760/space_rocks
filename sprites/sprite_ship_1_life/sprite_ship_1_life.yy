{
    "id": "cef111ed-ca6d-4619-8fff-32c3d399f0b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_ship_1_life",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db251821-63da-49ce-9ffa-97ec1945307e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cef111ed-ca6d-4619-8fff-32c3d399f0b9",
            "compositeImage": {
                "id": "f47f6b6e-f45b-4550-9c06-5e98c03dd36d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db251821-63da-49ce-9ffa-97ec1945307e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05f9426e-ff13-49fe-81aa-b0dbaa1bf9e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db251821-63da-49ce-9ffa-97ec1945307e",
                    "LayerId": "71207ab3-1963-451d-aa36-398fc713d8a3"
                }
            ]
        },
        {
            "id": "1e2f5b4d-3347-4793-a32a-ce0c59553f8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cef111ed-ca6d-4619-8fff-32c3d399f0b9",
            "compositeImage": {
                "id": "311b0cd2-c742-4dd7-82b5-cdd2844f7cd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e2f5b4d-3347-4793-a32a-ce0c59553f8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27a0e1e8-8ea6-4f69-ad55-9f5bee91fbbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e2f5b4d-3347-4793-a32a-ce0c59553f8a",
                    "LayerId": "71207ab3-1963-451d-aa36-398fc713d8a3"
                }
            ]
        },
        {
            "id": "01ae3c9d-6a00-4d29-874e-b3d3ad34d7cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cef111ed-ca6d-4619-8fff-32c3d399f0b9",
            "compositeImage": {
                "id": "79f05310-4929-4d0f-be5f-f0d5f01d6f81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01ae3c9d-6a00-4d29-874e-b3d3ad34d7cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "404c3dc5-e25b-4a15-9c36-ee594096b8eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01ae3c9d-6a00-4d29-874e-b3d3ad34d7cb",
                    "LayerId": "71207ab3-1963-451d-aa36-398fc713d8a3"
                }
            ]
        },
        {
            "id": "05702206-bc05-49ad-a44d-8bd4f57dae54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cef111ed-ca6d-4619-8fff-32c3d399f0b9",
            "compositeImage": {
                "id": "57253ff2-39c0-4a55-9686-adce7adce5f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05702206-bc05-49ad-a44d-8bd4f57dae54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb833819-76e1-4e84-bf01-45f48f03d420",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05702206-bc05-49ad-a44d-8bd4f57dae54",
                    "LayerId": "71207ab3-1963-451d-aa36-398fc713d8a3"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "71207ab3-1963-451d-aa36-398fc713d8a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cef111ed-ca6d-4619-8fff-32c3d399f0b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}