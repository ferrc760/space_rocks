{
    "id": "a02eb35f-a9e0-4814-be5f-f022799d2d83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22e6624d-a7d5-450b-8ef5-fdc06d740e07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a02eb35f-a9e0-4814-be5f-f022799d2d83",
            "compositeImage": {
                "id": "077b51c2-db62-4222-87b6-3799905315c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22e6624d-a7d5-450b-8ef5-fdc06d740e07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "501acc6c-4b0e-4be9-a407-c678eff464c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22e6624d-a7d5-450b-8ef5-fdc06d740e07",
                    "LayerId": "2ab0c832-01b0-4731-b801-1026b3cc80f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "2ab0c832-01b0-4731-b801-1026b3cc80f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a02eb35f-a9e0-4814-be5f-f022799d2d83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}