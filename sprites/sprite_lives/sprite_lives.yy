{
    "id": "5d0a20f2-f574-4815-8c1a-84b217894ac5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_lives",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "42a71fc0-e1c9-4e78-b6c3-db5788bc0ca6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d0a20f2-f574-4815-8c1a-84b217894ac5",
            "compositeImage": {
                "id": "4d4e42ec-62da-49f2-aeda-19690146f3fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42a71fc0-e1c9-4e78-b6c3-db5788bc0ca6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03c6042f-aa04-4b59-972e-1222ee45c695",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42a71fc0-e1c9-4e78-b6c3-db5788bc0ca6",
                    "LayerId": "9451a094-226a-4005-8246-eadd6d1d2dc4"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 16,
    "layers": [
        {
            "id": "9451a094-226a-4005-8246-eadd6d1d2dc4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d0a20f2-f574-4815-8c1a-84b217894ac5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}