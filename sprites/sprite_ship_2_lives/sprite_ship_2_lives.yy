{
    "id": "aa1c311a-1644-4680-aa5a-48f7cf504d42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_ship_2_lives",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ede0e643-17e5-4e84-a431-43a610c20f84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa1c311a-1644-4680-aa5a-48f7cf504d42",
            "compositeImage": {
                "id": "2c449ff2-9b4a-441f-b37d-cecbade11671",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ede0e643-17e5-4e84-a431-43a610c20f84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bbe8530-7a09-46fa-9bed-08ca1f04fe8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ede0e643-17e5-4e84-a431-43a610c20f84",
                    "LayerId": "86cb8835-ca14-496b-bb4b-0744e9d32b31"
                }
            ]
        },
        {
            "id": "a86acd6d-8e69-47f1-9495-2e87e21a05bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa1c311a-1644-4680-aa5a-48f7cf504d42",
            "compositeImage": {
                "id": "b5040dc5-74fd-4dc9-9676-1f48a133acd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a86acd6d-8e69-47f1-9495-2e87e21a05bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ad91a65-28af-443d-aa69-558bb4a76767",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a86acd6d-8e69-47f1-9495-2e87e21a05bc",
                    "LayerId": "86cb8835-ca14-496b-bb4b-0744e9d32b31"
                }
            ]
        },
        {
            "id": "c2dea7d0-7c1c-4cb0-989c-4f7edcba7e90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa1c311a-1644-4680-aa5a-48f7cf504d42",
            "compositeImage": {
                "id": "a3e6bca2-231c-4b92-b313-275b74b7d442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2dea7d0-7c1c-4cb0-989c-4f7edcba7e90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d7b460a-3b1e-4c60-a911-faf74186ad27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2dea7d0-7c1c-4cb0-989c-4f7edcba7e90",
                    "LayerId": "86cb8835-ca14-496b-bb4b-0744e9d32b31"
                }
            ]
        },
        {
            "id": "7c52b4c2-b500-41a9-8f2b-80cd253fc68b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa1c311a-1644-4680-aa5a-48f7cf504d42",
            "compositeImage": {
                "id": "d6798fec-a734-4230-b188-cd7fc4960c0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c52b4c2-b500-41a9-8f2b-80cd253fc68b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b76a118-608e-43f3-a3fd-800cc0e593c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c52b4c2-b500-41a9-8f2b-80cd253fc68b",
                    "LayerId": "86cb8835-ca14-496b-bb4b-0744e9d32b31"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "86cb8835-ca14-496b-bb4b-0744e9d32b31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa1c311a-1644-4680-aa5a-48f7cf504d42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}