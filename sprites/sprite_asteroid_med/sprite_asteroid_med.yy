{
    "id": "dce2b407-f3fe-4b8e-b17b-a4125d6cb1b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_asteroid_med",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24b6ffc0-d0e7-495b-b157-c688a0b0468d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dce2b407-f3fe-4b8e-b17b-a4125d6cb1b5",
            "compositeImage": {
                "id": "86fff565-a103-4db5-bdc4-1230efcfd231",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24b6ffc0-d0e7-495b-b157-c688a0b0468d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eb87e5c-fdd7-46fb-af68-c706cca0f3cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24b6ffc0-d0e7-495b-b157-c688a0b0468d",
                    "LayerId": "1deac728-1dcc-4a12-9fc4-7f1ad45fd4c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1deac728-1dcc-4a12-9fc4-7f1ad45fd4c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dce2b407-f3fe-4b8e-b17b-a4125d6cb1b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}