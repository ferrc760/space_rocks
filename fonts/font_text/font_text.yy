{
    "id": "5692df2e-68d8-4a01-9b2c-d253c12f65b5",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_text",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "074c757a-5a8f-4694-bd8d-3d3f7fa6583e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ed6b4ce8-4dd9-44d9-9c14-f0b665b36641",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 216,
                "y": 50
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "bbf1e044-ecdb-4242-a68c-ae8e686f1633",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 208,
                "y": 50
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "15e0ecc7-87f8-4a04-8fd7-366e30f9e7c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 192,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "35d5fea4-4544-4efe-baa0-5de92a18b381",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 180,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "bdc459cb-6f7d-483f-b254-c952d456e983",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 166,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "673500f1-48d5-44cd-a73a-6f390674faf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 154,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "214e44ed-66c1-4ecf-adcb-41a576c7a882",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 150,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f6c0bc3d-65ec-482f-abaf-f2f69f1c6e4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 142,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "e8327c29-b362-419b-9073-aff4327433a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 134,
                "y": 50
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "7c8392b7-5104-48f0-a180-1b3bb06c17b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 221,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6973603d-e5aa-40d5-bdd6-3c5bf6736fd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 124,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "0ef47b23-8728-4e54-a64f-ba56f752240d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 2,
                "shift": 4,
                "w": 3,
                "x": 107,
                "y": 50
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "1958ebfc-edfd-4de2-a4f3-316fc59e11c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 99,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "012c200b-6252-4830-9f5d-29787b246557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 94,
                "y": 50
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "536cb93c-4985-4594-b506-fd4b24c0b43a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 84,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b8ea5974-92b7-4fb8-b763-f46fbfb875d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 72,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b3ebfd97-b130-4c89-bbac-e293c60fad44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 64,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c8382aac-1d21-46ff-a8b2-4c21f10ec83e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 54,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "f2499e96-8d09-4946-b736-45a1f78177d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 44,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d7c88c56-a66f-4763-8b8f-50a137440b7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 32,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "f00dc08e-3e89-4331-9416-fca93a902b76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 112,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "eca6f2d9-6a96-4053-a82e-56a57af446b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 231,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c3ec928b-1efa-42cb-808e-898b0b69efad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 242,
                "y": 50
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "dd38cb78-6906-4d36-9462-b65ba7cf5572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a6584e6a-97b8-420f-a22f-b344a5e53f75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 231,
                "y": 74
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "02fe7f3e-8e2c-4f0a-8ac7-056bc6cdbfcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 226,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "028f15a8-f20c-4bf6-abeb-b44421c7edf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 220,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ac4896fd-db91-4fae-84d0-576f8777144f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 213,
                "y": 74
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "bd339bbe-2609-42d4-b8bc-de66500b4e17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 204,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "7176f884-70b3-462c-9b07-f7a9345eda9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 196,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "66bd22a9-0cc9-4151-b08b-f755102613f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 186,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c7b13e09-069c-4e12-abf3-3aea0d553151",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 170,
                "y": 74
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "102dfac8-2d2b-439f-8bde-cfabbc87f69d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 158,
                "y": 74
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "2c47eebb-f782-4350-88fc-174cc92f120b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 9,
                "x": 147,
                "y": 74
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "822b1e11-03cc-4571-87e4-592f6c025bee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 135,
                "y": 74
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d78dfca3-1f21-4a37-8e81-c0781f64bdef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 123,
                "y": 74
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2b74a675-4c61-414c-9b04-5667138fa70b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 112,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5a121aa2-b523-4f73-9ede-8bac906ba624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 101,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "877c0290-e4b5-439d-b04f-0000e0366a0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 88,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "dd031717-8e35-49cf-98c7-b75dc79728f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 75,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "47450b29-0c45-458f-bdb6-f6967083198f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 64,
                "y": 74
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "02313add-f292-4796-a786-6639b153b55c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 51,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "becbb9d0-3151-4784-81ce-e0bf22ed8964",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 9,
                "x": 40,
                "y": 74
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "afff56b5-3e2c-40c1-95a8-eb08d1e1e654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 29,
                "y": 74
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "702d6108-518e-44f3-92d9-6e2467ab03f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 13,
                "y": 74
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "158534a6-51b0-4f72-8f34-bfb4640336f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 17,
                "y": 50
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "cce09bab-a396-42ce-b722-b59bba6a7eab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a12d9df9-074e-44e3-b845-d909f3a4ba3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 237,
                "y": 26
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "353f1e4b-3bfa-45ea-98b8-da0d41670ac6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ccceaaa0-325d-4f58-bd20-f5c7af3bbf3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "4f2d9fbe-4c22-4530-a7d9-974bc354fb9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "ca41ecb0-8f77-42a9-988f-9fd02192df11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 12,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "20af8592-efa5-4b29-8b12-23b36be55cbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "3a929e06-3103-49a5-b81c-e981e8514de7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "38279c30-7752-4323-acaf-1d081b258d0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "0b049ebb-54bb-4ea5-ae5d-56cfceeb3e7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9e7f44c4-5b9b-490a-a438-d5815b330af0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "6aa570ff-735e-4bcb-a239-a791a8b90662",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "a6268cab-6244-4d97-8d76-572798f6b749",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "5b2ca34c-770e-455a-8f8f-7700afb6031d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "688a7398-0b2b-413f-9975-8df7a98c2916",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "8074f1a6-52a7-4309-abf5-1ad05842f636",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "48a84ce4-30bb-47a4-93fa-748db6e2ea1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 12,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c5cf6ad0-9dfd-45c0-9938-9a56c65e2944",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 4,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "fc9cbd5f-525a-407c-ac26-05e54b5452c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b14acb00-c7a5-46d2-ab82-fd9515c5b5b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "9f443640-0027-4f53-8ea1-31769a269b6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "643e528f-dc8a-4462-a65c-4bf5cbc6d58c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "805a1bc9-b252-425c-af28-01ef84aa079d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "b53007f5-10bb-4982-8713-005f32528c54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "efb8af99-9294-455d-8869-2494f5e10437",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 18,
                "y": 26
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e6af9be7-568c-4934-9d59-1a7ec110753d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 120,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3f1fdd82-df90-4aa3-b80c-ad089b1101ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 28,
                "y": 26
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1d2c3739-2376-4df7-98f6-a38c25b7e4dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 218,
                "y": 26
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "83e16fc4-2d2f-464e-9a45-78b95dd6b7cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 208,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "40226894-0f73-4409-9279-cfd3bb222e40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 203,
                "y": 26
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "ee5b74a5-31bb-4387-bbc0-ffd6898f416b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 189,
                "y": 26
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "53e20648-5e12-4d09-9dcc-92463c0c8462",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 179,
                "y": 26
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d813e7c2-089e-4c8b-b62e-490513545021",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 169,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "d7acb18a-59e3-4868-b708-775e0500562c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 159,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "13d446f1-f9a6-4c7d-90f2-e2ece4396b47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 149,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "7752c101-a033-47b3-8bec-f5b444703c32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 140,
                "y": 26
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "073dfce6-6d63-49e1-a42e-df4151c22720",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 227,
                "y": 26
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e689c6ea-6d3b-4be4-a661-73f7261749e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 130,
                "y": 26
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "5f285765-e8bc-427e-994d-fe0f620422de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 110,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "378c2a95-f04c-4798-9151-3eabbf8f06e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 100,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d667bb66-5768-4ea9-8bb5-4949a8c3a75e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 87,
                "y": 26
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "afd45995-e062-48f1-93c8-a18ce3c6f6fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 76,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "154c614c-19ab-42b1-8f23-5e99c9f0583f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 65,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "0c0f4c45-195a-46a6-b933-462047f5cd11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 54,
                "y": 26
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "60c90455-a157-4ddc-b3e9-32149c2085c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 46,
                "y": 26
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "b5d9bb01-722a-497a-90f8-7e049e6ebc71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 3,
                "shift": 7,
                "w": 3,
                "x": 41,
                "y": 26
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "55029105-78a2-4a40-93b5-faa15548a151",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 33,
                "y": 26
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c54a7f9f-fbd1-4ed1-83d8-6d8c9df02544",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 243,
                "y": 74
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "f12848fc-bb7a-4c96-80ba-d88a5efd5355",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 22,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 2,
                "y": 98
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}