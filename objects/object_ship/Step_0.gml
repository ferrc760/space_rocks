/// @description Insert description here
// You can write your code in this editor
if
(keyboard_check(ord("A")))
{
	image_angle    =
	image_angle + 5;
}
if
(keyboard_check(ord("D")))
{
	image_angle    =
	image_angle - 5;
}
if
(keyboard_check(ord("W")))
{
	motion_add(image_angle, 0.05);
	image_speed = 10
}

move_wrap(true, true, sprite_width / 2);

if (keyboard_check_pressed(vk_space))
{
	audio_play_sound(sound_zap, 1, false);
	var inst  =  instance_create_layer(x, y, "Instances", object_bullet);
	inst.direction = image_angle;
}
if image_index == 3 then image_speed = 0