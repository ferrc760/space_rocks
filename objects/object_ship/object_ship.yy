{
    "id": "7811a3c4-be38-444b-9188-b5bef6d13a34",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_ship",
    "eventList": [
        {
            "id": "53f1c4e5-22ee-423c-9536-dbd1565e3a3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7811a3c4-be38-444b-9188-b5bef6d13a34"
        },
        {
            "id": "7bb57c89-a2b8-4123-b007-51912dd5ea38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "15a97b14-c2e6-413b-91a3-ffcd66a7bbe9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7811a3c4-be38-444b-9188-b5bef6d13a34"
        },
        {
            "id": "a1652c68-ad5e-4240-a85e-8e576339cbe7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7811a3c4-be38-444b-9188-b5bef6d13a34"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "e9f90d68-9f24-4b66-abaa-467c227c9c87",
    "visible": true
}