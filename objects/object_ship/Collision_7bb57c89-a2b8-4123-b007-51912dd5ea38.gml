/// @description Insert description here
// You can write your code in this editor
audio_play_sound(sound_die, 1, false);
lives -= 1;
instance_destroy();

repeat(10)
{
	instance_create_layer(x, y, "Instances", object_debris);
}

with (object_game)
{
	alarm[1] = room_speed;
}
if lives == 2
{
	object_set_sprite(object_ship, sprite_ship_2_lives)
}
if lives == 1
{
	object_set_sprite(object_ship, sprite_ship_1_life)
}