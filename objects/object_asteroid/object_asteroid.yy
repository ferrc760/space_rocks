{
    "id": "15a97b14-c2e6-413b-91a3-ffcd66a7bbe9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_asteroid",
    "eventList": [
        {
            "id": "84afaf01-323f-497e-9d40-f133701d4978",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "15a97b14-c2e6-413b-91a3-ffcd66a7bbe9"
        },
        {
            "id": "d72dd4bb-5080-4526-ae37-0c912eaa2569",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "15a97b14-c2e6-413b-91a3-ffcd66a7bbe9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "732a8dfc-e161-4f3a-af34-3087f19afc40",
    "visible": true
}