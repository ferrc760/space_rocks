{
    "id": "83f3480b-5d5d-4047-bb87-1c74613a4e4f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_bullet",
    "eventList": [
        {
            "id": "8b09f19a-dcd6-4371-b927-c311f6373b7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "83f3480b-5d5d-4047-bb87-1c74613a4e4f"
        },
        {
            "id": "c01d9c84-de14-48cc-8877-3099c2679b20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "15a97b14-c2e6-413b-91a3-ffcd66a7bbe9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "83f3480b-5d5d-4047-bb87-1c74613a4e4f"
        },
        {
            "id": "e8f1ea60-cebd-4255-b14c-7ea02222ac70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "83f3480b-5d5d-4047-bb87-1c74613a4e4f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a02eb35f-a9e0-4814-be5f-f022799d2d83",
    "visible": true
}