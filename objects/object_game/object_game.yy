{
    "id": "56219ec0-1ed1-498e-a28a-8ea7b027fef0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_game",
    "eventList": [
        {
            "id": "a17fc0b7-7168-45c7-ab36-d92b6612caee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "56219ec0-1ed1-498e-a28a-8ea7b027fef0"
        },
        {
            "id": "9c4469d1-a290-4566-824f-81be0827cff3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "56219ec0-1ed1-498e-a28a-8ea7b027fef0"
        },
        {
            "id": "032dbf6e-ea42-454d-9b66-a451ce0d84da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "56219ec0-1ed1-498e-a28a-8ea7b027fef0"
        },
        {
            "id": "a024c402-bdc5-4775-b783-194fe866f765",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "56219ec0-1ed1-498e-a28a-8ea7b027fef0"
        },
        {
            "id": "79c5c3f0-0517-4cfb-817a-061e337c8000",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "56219ec0-1ed1-498e-a28a-8ea7b027fef0"
        },
        {
            "id": "f7daebcf-7e4a-4913-8040-482768fd9abb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "56219ec0-1ed1-498e-a28a-8ea7b027fef0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}