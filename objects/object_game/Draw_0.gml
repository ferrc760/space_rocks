/// @description Insert description here
// You can write your code in this editor
switch (room)
{
	case room_game:
	draw_text(45, 20, "SCORE: " + string(score));
	for (var i = 0; i < lives; i++) {
		draw_sprite(sprite_lives, 0, x +22 + i * 20, y +45);
	}
	break;
	
	case room_start:
	draw_set_halign(fa_center);
	var c = c_yellow;
	draw_text_transformed_colour(room_width / 2, 100, "SPACE ROCKS", 3, 3, 0, c, c, c, c, 1);
	draw_text(room_width / 2, 200, @"Score 1,000 points to win!

W: forward
A/D: change direction
SPACE: shoot

>>PRESS ENTER TO START<<")
	break;
	
	case room_win:
		for (var i = 0; i < lives; i++) {
			draw_sprite(sprite_lives, 0, x + 225 + i * 20, y +260);
			
		}
		draw_set_halign(fa_center)
		var c = c_lime;
		draw_text_transformed_colour(room_width / 2, 150, "YOU WON!", 3, 3, 0, c, c, c, c, 1);
		draw_text(room_width / 2, 300, "PRESS ENTER TO RESTART");
		draw_set_halign(fa_left);
	
	break;
	
	case room_gameover:
	draw_set_halign(fa_center)
		var c = c_red;
		if score <= 0
		{
		draw_text_transformed_colour(room_width / 2, 150, "YOU ARE A LOSER!", 3, 3, 0, c, c, c, c, 1);
		
		}
		else
		{ 
			draw_text_transformed_colour(room_width / 2, 150, "GAME OVER", 3, 3, 0, c, c, c, c, 1);
		}
		draw_text(room_width / 2, 250, "FINAL SCORE: " + string(score));
		draw_text(room_width / 2, 300, "PRESS ENTER TO RESTART");
		draw_set_halign(fa_left);
	break;
}