{
    "id": "5979e8f5-8614-4b56-b0b3-3ddd6ebb2c47",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_debris",
    "eventList": [
        {
            "id": "7849acc3-b3e1-4042-9024-6172d3d8c175",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5979e8f5-8614-4b56-b0b3-3ddd6ebb2c47"
        },
        {
            "id": "7bf7e351-6d7d-4a05-ad8b-6565a1265755",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5979e8f5-8614-4b56-b0b3-3ddd6ebb2c47"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9885bb5f-93ff-44f6-ab07-1fb101e9d1fe",
    "visible": true
}